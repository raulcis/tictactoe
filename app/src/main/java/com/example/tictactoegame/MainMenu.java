package com.example.tictactoegame;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenu extends AppCompatActivity {

    Button play,HowTo;
    private Bitmap background;
    Canvas canvas = new Canvas();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        play = (Button) findViewById(R.id.PlayButton);
        HowTo = (Button) findViewById(R.id.HowToButton);
        MenuButtons();

        background = BitmapFactory.decodeResource(getResources(), R.drawable.gamecover);
        background = Bitmap.createScaledBitmap(background, Resources.getSystem().getDisplayMetrics().widthPixels, Resources.getSystem().getDisplayMetrics().heightPixels , true);
        canvas.drawBitmap(background, 0, 0, null);
    }

    private void MenuButtons(){
        play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainMenu.this,MainActivity.class));
            }
        });

        HowTo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainMenu.this,HowToPlay.class));
            }
        });

    }
}