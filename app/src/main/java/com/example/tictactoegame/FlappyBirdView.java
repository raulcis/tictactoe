package com.example.tictactoegame;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import java.util.Random;

public class FlappyBirdView extends View {

    int score;
    private AlertDialog.Builder alertDialog;
    private FlappyBirdActivity birdActivity;
    private boolean isGameOver = false;


    private Handler handler; //lets you schedule a runnable after some delay
    private Runnable runnable;
    private final int UPDATE_MILLISECOND = 30;
    private Bitmap background;
    private Display display;
    private Point point;
    private int dWidth, dHeight;
    private Rect rect;
    //Lets create a Bitmap array for the bird
    private Bitmap[] birds;
    //We need integer variable to track the bird image frame
    private int birdFrame= 0;
    private int velocity= 0;
    private int gravity = 3;
    private int mPosX, mPosY;
    private boolean gameState = true;
    private boolean alive = true;

    //variables for the pipe
    private Bitmap topPipe, bottomPipe;
    private int gap =400;
    private int minPipeOffset, maxPipeOffset;
    private int NoOfPipes = 4;
    private int distanceBetweenPipes;
    private int[] PipeX = new int [NoOfPipes];
    private int[] PipeTopY = new int [NoOfPipes];
    private int PipeVelocity = 8;
    Random random;





    public FlappyBirdView(Context context){
        super(context);
        birdActivity = (FlappyBirdActivity) context;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate();// this calls OnDraw()
            }
        };

        score = 0;

        background = BitmapFactory.decodeResource(getResources(),R.drawable.flappy_bird_background);
        topPipe = BitmapFactory.decodeResource(getResources(),R.drawable.toppipe);
        topPipe = Bitmap.createScaledBitmap(topPipe, 140, 1280, true);
        bottomPipe = BitmapFactory.decodeResource(getResources(),R.drawable.bottompipe);
        bottomPipe = Bitmap.createScaledBitmap(bottomPipe, 140, 1280, true);
        display = ((Activity)getContext()).getWindowManager().getDefaultDisplay();
        point = new Point();
        display.getSize(point);
        dHeight = point.y;
        dWidth = point.x;
        rect = new Rect(0,0,dWidth,dHeight);
        birds = new Bitmap[3];
        birds[0] = BitmapFactory.decodeResource(getResources(), R.drawable.flappy_bird1);
        birds[0] = Bitmap.createScaledBitmap(birds[0], 140, 100, true);
        birds[1] = BitmapFactory.decodeResource(getResources(), R.drawable.flappy_bird2);
        birds[1] = Bitmap.createScaledBitmap(birds[1], 140, 100, true);
        birds[2] = BitmapFactory.decodeResource(getResources(), R.drawable.flappy_bird3);
        birds[2] = Bitmap.createScaledBitmap(birds[2], 140, 100, true);
        mPosX = (dWidth - birds[birdFrame].getWidth()) / 2;
        mPosY = (dHeight - birds[birdFrame].getHeight()) / 2;

        //initialize pipes
        random = new Random();
        distanceBetweenPipes = dWidth * 3 / 4;
        minPipeOffset = gap / 2;
        maxPipeOffset = dHeight - minPipeOffset - gap;
        for(int i = 0; i< NoOfPipes; i++){
            PipeX[i] = dWidth + i * distanceBetweenPipes;
            PipeTopY[i] = minPipeOffset + random.nextInt(maxPipeOffset - minPipeOffset + 1);

        }
//        PipeX = dWidth / 2 - topPipe.getWidth()/2;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        alive = isAlive();
        if(alive){
            //draw background
            canvas.drawBitmap(background,null,rect,null);// adjust size according to the window

            //draw bird flapping
            //putting the bird in the center of the screen. Making the
            birdFrame = (birdFrame + 1) % 3;// counting frames.

            if(gameState){
                //Check if the bird did not reach the bottom of the screen and update velocity
                if(mPosY< dHeight - birds[0].getHeight() || velocity < 0){
                    velocity += gravity;
                    mPosY += velocity;
                }
            }
            for(int i = 0 ; i < NoOfPipes; i++){
                PipeX[i] -= PipeVelocity;
                if(PipeX[i] < -topPipe.getWidth() ){
                    PipeX[i] += NoOfPipes * distanceBetweenPipes;
                    PipeTopY[i] = minPipeOffset + random.nextInt(maxPipeOffset - minPipeOffset + 1);

                }
                canvas.drawBitmap(topPipe, PipeX[i], PipeTopY[i] - topPipe.getHeight(),null);
                canvas.drawBitmap(bottomPipe,PipeX[i],PipeTopY[i] + gap,null);
            }

        }else{
            if (isGameOver) {
                return;
            } else {
                isGameOver = true;
            }


            alertDialog = new AlertDialog.Builder(birdActivity);
            alertDialog.setTitle("GAME OVER");
            alertDialog.setMessage("Score: " + String.valueOf(this.score) );
            alertDialog.setCancelable(false);
            birdActivity.UpdateScore();

            alertDialog.setNegativeButton("CONTINUE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    birdActivity.onBackPressed();
                }
            });
            alertDialog.show();
        }



        canvas.drawBitmap(birds[birdFrame],mPosX, mPosY,null);



        handler.postDelayed(runnable,UPDATE_MILLISECOND);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){

        int action = event.getAction();
        if(action == MotionEvent.ACTION_DOWN){//when Tap is detected
            //update the velocity of the bird;
            velocity = -30;
            gameState = true;

        }

        return true; // no further action done
    }

    public boolean isAlive() {
        // Check if the bird hits the pipes
        for (int i = 0; i < NoOfPipes; i++) {
            if ((PipeX[i] >= dWidth / 2 - 53 / 2.0f - birds[0].getWidth()/ 2) &&
                    (PipeX[i] <= dWidth / 2 + 53 / 2.0f + birds[0].getWidth() / 2)) {
                if ((mPosY <= PipeTopY[i]) ||
                        (mPosY >= PipeTopY[i] + gap - birds[0].getHeight())) {

                    Log.d("Position check", "Y: " + mPosY + "\t Upper: " +  PipeTopY[i]
                            + "\t Bottom : " + (PipeTopY[i] + gap - birds[0].getHeight()));
                    return false;
                } else {
                    if (PipeX[i] - PipeVelocity <
                            dWidth / 2.0f - 53 / 2.0f - birds[0].getWidth() / 2.0f) {
                        score++;

                        // Update the score in MainActivity
                        Log.d("SCORE", Integer.toString(score));

                    }
                }
            }
        }
        // Check if the bird goes beyond the border
        if ((mPosY < 0.0f + birds[0].getHeight() / 2.0f) || (mPosY > dHeight - birds[0].getHeight()  / 2.0f)) {
            return false;
        }

        return true;
    }



}



//public class FlappyBirdView extends SurfaceView implements SurfaceHolder.Callback{
//
//
//    /**
//     *     how many elements should be in this view?
//     *     1. a bird that moves around the screen
//     *     2. pipe, or a list of pipes
//     *     3. the background (can be dealt in the xml file)
//     *     4. The position of the bird : X, Y
//     *     5. The width and height of the game
//     */
//
//    private SurfaceHolder surfaceHolder;
//
//    //screenDimension
//    private float Width;
//    private float Height;
//
//
//    //information about the bird
//    private Bitmap birdbitmap;
//    private Bird bird;
//    private int dimen = 100;
//
//
//
//    //information about the
//    private Bitmap pipe;
//    private int iteratorInt = 0;
//    private static final int interval = 150;
//    private static final float gap = 450.0f;
//    private static final float base = 100.0f;
//    private float pipeWidth = 100.0f;
////    private List<Pipe> pipeList;
//    private static final float pipeVelocity = 3.0f;
//
//
//
//
//    public FlappyBirdView(Context context) {
//        super(context);
//
//        surfaceHolder = getHolder();
//        surfaceHolder.addCallback(this);
//
//        setZOrderOnTop(true);
//        surfaceHolder.setFormat(PixelFormat.TRANSPARENT);
//
//
//        //initialize bird
//        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.bird_pixel);
//        birdbitmap = Bitmap.createScaledBitmap(b, 100, 100, false);
//
//        //temp
//        Canvas canvas = surfaceHolder.lockCanvas();
//        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//        canvas.drawBitmap(birdbitmap, Width / 2.0f, Height / 2.0f,null );
//
//
//
//        setKeepScreenOn(true);
//
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//
//        Height = w;
//        Width = h;
//
//        bird.setmPos(Width / 2.0f, Height / 2.0f);
//
//        //initialize pipes again.
//    }
//
//
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//        canvas.drawBitmap(birdbitmap, Width / 2.0f, Height / 2.0f,null );
//    }
//
//
//    @Override
//    public void surfaceCreated(@NonNull SurfaceHolder holder) {
//
//    }
//
//    @Override
//    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
//
//    }
//
//    @Override
//    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
//
//    }
//}
