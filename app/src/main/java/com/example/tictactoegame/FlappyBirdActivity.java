package com.example.tictactoegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class  FlappyBirdActivity extends AppCompatActivity {

    FlappyBirdView birdView;

    public final static String SCORE = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        birdView = new FlappyBirdView(this);
        setContentView(birdView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void UpdateScore(){
        Intent score = new Intent();
        setResult(RESULT_OK, score);
        score.putExtra(SCORE,birdView.score);
    }
}