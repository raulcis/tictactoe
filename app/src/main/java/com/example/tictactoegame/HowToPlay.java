package com.example.tictactoegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HowToPlay extends AppCompatActivity {

    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
        back = (Button) findViewById(R.id.Back);
        back();
    }

    private void back(){
        back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
              finish();
            }
        });

    }
}