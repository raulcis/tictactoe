package com.example.tictactoegame;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.session.MediaSession;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView playerOneScore, playerTwoScore, playerOneScoreFlip, playerTwoScoreFlip, playerStatus, playerStatusFlip;
    private Button[] buttons = new Button[9];
    private Button challengeOne, challengeTwo;
    private boolean pressedButton, wonMiniGame = true;
    int prevbtn;
    private static final int MEMORYGAME_REQUEST_CODE = 0;
    private static final int NUMBERSGAME_REQUEST_CODE = 1;
    private static final int FLAPPYBIRDGAME_REQUEST_CODE = 2;
    Random rand = new Random();

    String changedButtonID;
    int gameStatePointer;

    private int playerOneScoreCount, playerTwoScoreCount, roundCount;
    boolean activePlayer;

    // p1 -> 0 p2 -> 1 empty -> 2
    int[] gameState = {2, 2, 2, 2, 2, 2, 2, 2, 2};

    int[][] winningPositions = {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, //rows
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, // columns
            {0, 4, 8}, {2, 4, 6} // diagnal
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerOneScore = (TextView) findViewById(R.id.playerOneScore);
        playerTwoScore = (TextView) findViewById(R.id.playerTwoScore);
        playerOneScoreFlip = (TextView) findViewById(R.id.playerOneScoreFlip);
        playerTwoScoreFlip = (TextView) findViewById(R.id.playerTwoScoreFlip);
        playerStatus = (TextView) findViewById(R.id.playerStatus);
        playerStatusFlip = (TextView) findViewById(R.id.playerStatusFlip);
        challengeOne = (Button) findViewById(R.id.ChallengeOne);
        challengeTwo = (Button) findViewById(R.id.ChallengeTwo);
        //  resetGame = (Button) findViewById(R.id.resetGame);

        for (int i = 0; i < buttons.length; i++) {
            String buttonID = "btn" + i;
            int resourceID = getResources().getIdentifier(buttonID, "id", getPackageName());
            buttons[i] = (Button) findViewById(resourceID);
            buttons[i].setOnClickListener(this);
        }

        roundCount = 0;
        playerOneScoreCount = 0;
        playerTwoScoreCount = 0;
        activePlayer = true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gamemenu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Restart:
                ResetGame();
                return true;
            case R.id.HowToPlay:
                ShowHowToPlay();
                return true;
            case R.id.MainMenu:
                ReturnToMenu();
                return true;

            default:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {


        if (roundCount < 9) {

            if (!((Button) view).getText().toString().equals("")) {
                return;
            }

            String buttonID = view.getResources().getResourceEntryName(view.getId());
            int gameStatePointer = Integer.parseInt(buttonID.substring(buttonID.length() - 1, buttonID.length()));

            if (activePlayer) {
                ((Button) view).setText("X");
                ((Button) view).setTextColor(Color.parseColor("#162FEC"));
                gameState[gameStatePointer] = 0;
            } else {
                ((Button) view).setText("O");
                ((Button) view).setTextColor(Color.parseColor("#9C16EC"));
                gameState[gameStatePointer] = 1;
            }
            roundCount++;

            if (checkWinner()) {
                if (activePlayer) {
                    playerOneScoreCount++;
                    updatePlayerScore();
                    Toast.makeText(this, "Player one won!", Toast.LENGTH_SHORT).show();
                    if(playerOneScoreCount == 3){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Games Over!");
                        builder.setMessage("Player One Wins!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.create().show();
                    }
                    playAgain();
                } else {
                    playerTwoScoreCount++;
                    updatePlayerScore();
                    Toast.makeText(this, "Player two won!", Toast.LENGTH_SHORT).show();
                    if(playerTwoScoreCount == 3){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Games Over!");
                        builder.setMessage("Player Two Wins!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                        builder.create().show();

                    }
                    playAgain();
                }
            } else {
                activePlayer = !activePlayer;
            }

            if (playerOneScoreCount > playerTwoScoreCount) {
                playerStatus.setText("Player one is winning!");
                playerStatusFlip.setText("Player one is winning!");
            } else if (playerOneScoreCount < playerTwoScoreCount) {
                playerStatus.setText("Player two is winning!");
                playerStatusFlip.setText("Player two is winning!");
            } else {
                playerStatus.setText("");
                playerStatusFlip.setText("");
            }
        } else {

            if (activePlayer) {

                challengeTwo.setBackgroundColor(Color.parseColor("#70706F"));
                challengeTwo.setAlpha((float) 0.20);

                if (((Button) view).getText() == "O") {

                    challengeOne.setBackgroundColor(Color.GREEN);
                    challengeOne.setAlpha(1);

                    if (pressedButton == false) {
                        prevbtn = ((Button) view).getId();
                        ((Button) view).setBackgroundColor(Color.YELLOW);
                        pressedButton = true;
                    } else if (pressedButton == true) {
                        for (int i = 0; i < buttons.length; i++) {
                            if (buttons[i].getId() == prevbtn) {
                                buttons[i].setBackgroundColor(Color.parseColor("#62EDF7"));
                            }
                        }
                        prevbtn = ((Button) view).getId();
                        ((Button) view).setBackgroundColor(Color.YELLOW);
                    }
                }
            } else {

                challengeOne.setBackgroundColor(Color.parseColor("#70706F"));
                challengeOne.setAlpha((float) 0.20);

                if (((Button) view).getText() == "X") {

                    challengeTwo.setBackgroundColor(Color.GREEN);
                    challengeTwo.setAlpha(1);

                    if (pressedButton == false) {
                        prevbtn = ((Button) view).getId();
                        ((Button) view).setBackgroundColor(Color.YELLOW);
                        pressedButton = true;
                    } else if (pressedButton == true) {
                        for (int i = 0; i < buttons.length; i++) {
                            if (buttons[i].getId() == prevbtn) {
                                buttons[i].setBackgroundColor(Color.parseColor("#62EDF7"));
                            }
                        }
                        prevbtn = ((Button) view).getId();
                        ((Button) view).setBackgroundColor(Color.YELLOW);
                    }
                }
            }

            if (playerOneScoreCount > playerTwoScoreCount) {
                playerStatus.setText("Player one is winning!");
                playerStatusFlip.setText("Player one is winning!");
            } else if (playerOneScoreCount < playerTwoScoreCount) {
                playerStatus.setText("Player two is winning!");
                playerStatusFlip.setText("Player two is winning!");
            } else {
                playerStatus.setText("");
                playerStatusFlip.setText("");
            }


        }


        challengeOne.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (activePlayer == true) {

                    // call minigames here


                    int randNum = rand.nextInt(3);
                    Intent intent;
                    if(randNum == 0) {
                        intent = new Intent(MainActivity.this, NumbersGame.class);
                        startActivityForResult(intent, NUMBERSGAME_REQUEST_CODE);
                    } else if(randNum == 1) {
                        intent = new Intent(MainActivity.this,MemoryGame.class);
                        startActivityForResult(intent, MEMORYGAME_REQUEST_CODE);
                    } else if (randNum == 2) {
                        intent = new Intent(MainActivity.this,FlappyBirdGame.class);
                        startActivityForResult(intent, FLAPPYBIRDGAME_REQUEST_CODE);
                    }else{
                        intent = null;
                    }

                    changedButtonID = view.getResources().getResourceEntryName(view.getId());
                    gameStatePointer = Integer.parseInt(changedButtonID.substring(changedButtonID.length() - 1, changedButtonID.length()));

                    challengeOne.setBackgroundColor(Color.parseColor("#70706F"));
                    challengeOne.setAlpha((float) 0.20);

                    for (int i = 0; i < buttons.length; i++) {
                        if (buttons[i].getId() == prevbtn) {
                            buttons[i].setBackgroundColor(Color.parseColor("#62EDF7"));
                        }
                    }
                }
            }
        });

        challengeTwo.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (activePlayer == false) {
                    //call minigames here

                    int randNum = rand.nextInt(3);
                    Intent intent;
                    if(randNum == 0) {
                        intent = new Intent(MainActivity.this, NumbersGame.class);
                        startActivityForResult(intent, NUMBERSGAME_REQUEST_CODE);
                    } else if(randNum == 1) {
                        intent = new Intent(MainActivity.this,MemoryGame.class);
                        startActivityForResult(intent, MEMORYGAME_REQUEST_CODE);
                    } else if (randNum == 2) {
                        intent = new Intent(MainActivity.this,FlappyBirdGame.class);
                        startActivityForResult(intent, FLAPPYBIRDGAME_REQUEST_CODE);
                    }else{
                        intent = null;
                    }

                    changedButtonID = view.getResources().getResourceEntryName(view.getId());
                    gameStatePointer = Integer.parseInt(changedButtonID.substring(changedButtonID.length() - 1, changedButtonID.length()));

                    challengeTwo.setBackgroundColor(Color.parseColor("#70706F"));
                    challengeTwo.setAlpha((float) 0.20);

                    for (int i = 0; i < buttons.length; i++) {
                        if (buttons[i].getId() == prevbtn) {
                            buttons[i].setBackgroundColor(Color.parseColor("#62EDF7"));
                        }
                    }
                }
            }
        });
    }

    public boolean checkWinner() {
        boolean winnerResult = false;

        for (int[] winningPosition : winningPositions) {
            if (gameState[winningPosition[0]] == gameState[winningPosition[1]] &&
                    gameState[winningPosition[1]] == gameState[winningPosition[2]] &&
                    gameState[winningPosition[0]] != 2) {
                winnerResult = true;
            }
        }
        return winnerResult;
    }

    public void winnerIf() {

        if (checkWinner()) {
            if (activePlayer) {
                playerOneScoreCount++;
                updatePlayerScore();
                Toast.makeText(getApplicationContext(), "Player one won!", Toast.LENGTH_SHORT).show();
                if(playerOneScoreCount == 3){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Games Over!");
                    builder.setMessage("Player One Wins!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
                    builder.create().show();
                }
                playAgain();
            } else {
                playerTwoScoreCount++;
                updatePlayerScore();
                Toast.makeText(getApplicationContext(), "Player two won!", Toast.LENGTH_SHORT).show();
                if(playerTwoScoreCount == 3){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Games Over!");
                    builder.setMessage("Player Two Wins!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
                    builder.create().show();
                }
                playAgain();
            }
        }


    }

    public void updatePlayerScore() {
        playerOneScore.setText(Integer.toString(playerOneScoreCount));
        playerTwoScore.setText(Integer.toString(playerTwoScoreCount));
        playerOneScoreFlip.setText(Integer.toString(playerOneScoreCount));
        playerTwoScoreFlip.setText(Integer.toString(playerTwoScoreCount));
    }

    public void playAgain() {
        roundCount = 0;
        activePlayer = true;
        pressedButton = false;
        prevbtn = 0;

        for (int i = 0; i < buttons.length; i++) {
            gameState[i] = 2;
            buttons[i].setText("");
        }

        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setBackgroundColor(Color.parseColor("#62EDF7"));
        }
    }

    public void ResetGame(){
        playAgain();
        playerOneScoreCount = 0;
        playerTwoScoreCount = 0;
        playerStatus.setText("");
        updatePlayerScore();
    }

    public void ReturnToMenu(){
        ResetGame();
        finish();
    }

    public void ShowHowToPlay(){
        startActivity(new Intent(MainActivity.this,HowToPlay.class));
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MEMORYGAME_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                boolean playerOneWin = data.getBooleanExtra("winner",false);
                if (playerOneWin == true) {
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("X");
                    changeBtn.setTextColor(Color.parseColor("#162FEC"));
                    gameState[gameStatePointer] = 0;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }else if (playerOneWin == false){
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("O");
                    changeBtn.setTextColor(Color.parseColor("#9C16EC"));
                    gameState[gameStatePointer] = 1;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }
            }
        }
        ////////////////////////////////////////////////////////////////////////////////

        if (requestCode == NUMBERSGAME_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                boolean playerOneWin = data.getBooleanExtra("winner",false);
                if (playerOneWin == true) {
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("X");
                    changeBtn.setTextColor(Color.parseColor("#162FEC"));
                    gameState[gameStatePointer] = 0;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }else if (playerOneWin == false){
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("O");
                    changeBtn.setTextColor(Color.parseColor("#9C16EC"));
                    gameState[gameStatePointer] = 1;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }
            }
        }

        ///////////////////////////////////////////////////////////////////////////////

        if (requestCode == FLAPPYBIRDGAME_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                boolean playerOneWin = data.getBooleanExtra("winner",false);
                if (playerOneWin == true) {
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("X");
                    changeBtn.setTextColor(Color.parseColor("#162FEC"));
                    gameState[gameStatePointer] = 0;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }else if (playerOneWin == false){
                    int resourceID = getResources().getIdentifier(changedButtonID, "id", getPackageName());
                    Button changeBtn = (Button) findViewById(resourceID);

                    changeBtn.setText("O");
                    changeBtn.setTextColor(Color.parseColor("#9C16EC"));
                    gameState[gameStatePointer] = 1;

                    if(checkWinner()) {
                        winnerIf();
                    }else if (checkWinner() == false){
                        // after minigame is done switch player turn
                        activePlayer = !activePlayer;
                    }

                }
            }
        }



    }
}