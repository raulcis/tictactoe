package com.example.tictactoegame;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class FlappyBirdGame extends AppCompatActivity {


    private boolean player1 = true;
    private int player1score = 0;
    private int player2score = 0;
    private final int PLAYER1SCORE = 0;
    private final int PLAYER2SCORE = 1;
    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flappy_bird_game);
    }

    public void StartGame(View view) {
        if (player1) {
            Intent intent = new Intent(this, FlappyBirdActivity.class);
            startActivityForResult(intent, PLAYER1SCORE);
            TextView player = (TextView) findViewById(R.id.player);
            player.setText("Player 2");

            player1 = false;
        } else {

            Intent intent2 = new Intent(this, FlappyBirdActivity.class);
            startActivityForResult(intent2,PLAYER2SCORE);

//            finish();

        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Debugging", "onActivityResult Called");
        if (requestCode == PLAYER1SCORE) {
            Log.d("Debugging", "onActivityResult Called1.5");

            if (resultCode == RESULT_OK) {
                Log.d("Debugging", "onActivityResult Called2");
                player1score = data.getIntExtra(FlappyBirdActivity.SCORE, 0);
                Log.d("BirdGameActivity", "player 1 score: " + player1score);
            }
        }else if (requestCode == PLAYER2SCORE){
            Log.d("Debugging", "onActivityResult Called3");
            if (resultCode == RESULT_OK) {
                Log.d("Debugging", "onActivityResult Called4");
                player2score = data.getIntExtra(FlappyBirdActivity.SCORE, 0);
                Log.d("BirdGameActivity", "player 2 score: " + player2score);


                determineWinner();
            }
        }
    }

    private void determineWinner(){


        if(player1score == player2score){
            player1 = true;
            TextView player = (TextView) findViewById(R.id.player);
            player.setText("Player 1");

            alertDialog = new AlertDialog.Builder(FlappyBirdGame.this);
            alertDialog.setTitle("Game Result");
            String result_string = "Player 1 : " + player1score + "\t" + "Player 2 :" +player2score;
            result_string = result_string + "\nDraw. Repeat the game session";
            alertDialog.setMessage(result_string);
            alertDialog.setCancelable(false);

            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog.show();

        }else{
            Intent resultIntent = new Intent();
            resultIntent.putExtra("winner",player1score > player2score);
            setResult(RESULT_OK, resultIntent);

            alertDialog = new AlertDialog.Builder(FlappyBirdGame.this);
            alertDialog.setTitle("Game Result");
            String result_string = "Player 1 : " + player1score + "\t" + "Player 2 :" +player2score;
            String winner = player1score > player2score? "Player 1" : "Player 2";
            result_string = result_string + "\n" + winner + " won! Going back to main game";
            alertDialog.setMessage(result_string);
            alertDialog.setCancelable(false);

            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alertDialog.show();
        }

    }
}