package com.example.tictactoegame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class NumbersGame extends AppCompatActivity implements View.OnClickListener {

    final int MAX = 17;
    int score;
    boolean playerOneTurn;
    private TextView playerText, playerView, scoreView, playerText2, playerView2, scoreView2;
    private Button sub1, sub2, mul3, div2, sub12, sub22, mul32, div22;
    private HashMap<Button, Integer> uses;
    //improve UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers_game);
        score = MAX;
        playerOneTurn = true;
        playerText = (TextView) findViewById(R.id.playerTextView);
        playerView = (TextView) findViewById(R.id.playerView);
        scoreView = (TextView) findViewById(R.id.scoreView);
        sub1 = (Button) findViewById(R.id.sub1);
        sub2 = (Button) findViewById(R.id.sub2);
        mul3 = (Button) findViewById(R.id.mul3);
        div2 = (Button) findViewById(R.id.div2);

        playerText2 = (TextView) findViewById(R.id.playerTextView2);
        playerView2 = (TextView) findViewById(R.id.playerView2);
        scoreView2 = (TextView) findViewById(R.id.scoreView2);
        sub12 = (Button) findViewById(R.id.sub12);
        sub22 = (Button) findViewById(R.id.sub22);
        mul32 = (Button) findViewById(R.id.mul32);
        div22 = (Button) findViewById(R.id.div22);

        uses = new HashMap<>();

        updateText();

        uses.put(sub1, 99);
        uses.put(sub2, 99);
        uses.put(div2, 6);
        uses.put(mul3, 2);

        uses.put(sub12, 99);
        uses.put(sub22, 99);
        uses.put(div22, 6);
        uses.put(mul32, 2);

        sub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(sub1) > 0 && playerOneTurn == true) {
                    score -= 1;
                    uses.put(sub1, uses.get(sub1) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(sub1) <= 0 && playerOneTurn == true){
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        sub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(sub2) > 0 && playerOneTurn == true) {
                    score -= 2;
                    uses.put(sub2, uses.get(sub2) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if(uses.get(sub2) <= 0 && playerOneTurn == true){
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        div2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(div2) > 0 && playerOneTurn == true) {
                    score /= 2;
                    uses.put(div2, uses.get(div2) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(div2) <= 0 && playerOneTurn == true) {
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        mul3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(mul3) > 0 && playerOneTurn == true) {
                    score *= 3;
                    uses.put(mul3, uses.get(mul3) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(mul3) <= 0 && playerOneTurn == true) {
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        sub12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(sub12) > 0 && playerOneTurn == false) {
                    score -= 1;
                    uses.put(sub12, uses.get(sub12) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(sub12) <= 0 && playerOneTurn == false){
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        sub22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(sub22) > 0 && playerOneTurn == false) {
                    score -= 2;
                    uses.put(sub22, uses.get(sub22) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(sub22) <= 0 && playerOneTurn == false){
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        div22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(div22) > 0 && playerOneTurn == false) {
                    score /= 2;
                    uses.put(div22, uses.get(div22) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if (uses.get(div22) <= 0 && playerOneTurn == false) {
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        mul32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uses.get(mul32) > 0 && playerOneTurn == false) {
                    score *= 3;
                    uses.put(mul32, uses.get(mul32) - 1);
                    checkWinner(score);
                    playerOneTurn = !(playerOneTurn);
                } else if(uses.get(mul32) <= 0 && playerOneTurn == false){
                    Toast.makeText(getApplicationContext(), "No more uses left", Toast.LENGTH_SHORT).show();
                }
                updateButtons();
                updateText();
            }
        });

        updateButtons();
    }


    @Override
    public void onClick(View view) {

    }

    public void checkWinner(int score){
        if(score <= 0) {
            updateText();
            System.out.println("Player " + (playerOneTurn ? 1 : 2) + " won!");
            playerText.setText("Player " + (playerOneTurn ? 1 : 2) + " won!");
            //move back to main activity with info on who won
            Intent intent = new Intent();
            //Intent sets the winner as the current player
            intent.putExtra("winner", playerOneTurn);
            setResult(RESULT_OK, intent);
            finish();

        } else {
            updateText();
        }
    }

    public void updateText() {
        playerView.setText("Player " + (playerOneTurn ? 1 : 2) + "'s turn");
        scoreView.setText("Score: " + score);
        playerView2.setText("Player " + (playerOneTurn ? 1 : 2) + "'s turn");
        scoreView2.setText("Score: " + score);
    }

    public void updateButtons() {
        sub1.setText(getResources().getString(R.string.sub1) + "\n Uses: " + uses.get(sub1));
        sub2.setText(getResources().getString(R.string.sub2) + "\n Uses: " + uses.get(sub2));
        mul3.setText(getResources().getString(R.string.mul3) + "\n Uses: " + uses.get(mul3));
        div2.setText(getResources().getString(R.string.div2) + "\n Uses: " + uses.get(div2));

        sub12.setText(getResources().getString(R.string.sub1) + "\n Uses: " + uses.get(sub12));
        sub22.setText(getResources().getString(R.string.sub2) + "\n Uses: " + uses.get(sub22));
        mul32.setText(getResources().getString(R.string.mul3) + "\n Uses: " + uses.get(mul32));
        div22.setText(getResources().getString(R.string.div2) + "\n Uses: " + uses.get(div22));
    }
}
