package com.example.tictactoegame;

public class Bird {

    public float mPosX = 0.0f;
    public float mPosY = 0.0f;
    private float mVelX = 0.0f;
    private float mVelY = 0.0f;

    /**
     * Calculate displacement of the particle along the X and Y axis
     * @param x acceleration of x-axis
     * @param y acceleration of x-axis
     * @param z acceleration of x-axis
     * @param timestamp timestamp of the sensor event
     */
    public void updatePosition(float x, float y, float z, long timestamp) {
        float dt = (System.nanoTime() - timestamp) / 100000000.0f;
        mVelX += -x * dt;
        mVelY += -y * dt;
        mPosX += mVelX * dt;
        mPosY += mVelY * dt;
    }


    /**
     * A method to enable the jumping
     * @param yVel vertical velocity of the bird when jumping
     */
    public void jump(float yVel){
        mVelY = yVel;

    }

    /**
     * method for setting positions of the bird
     * @param mPosX x-axis position
     * @param mPosY y-axis position
     */
    public void setmPos(float mPosX, float mPosY) {
        this.mPosX = mPosX;
        this.mPosY = mPosY;
    }

}
