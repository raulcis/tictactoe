package com.example.tictactoegame;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.tictactoegame.databinding.MemorygameBinding;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;


public class MemoryGame extends AppCompatActivity {

    private MemorygameBinding binding;
    Handler handler = new Handler();
    private ArrayList<Integer> solution;
    private ArrayList<Integer> player1Solution;
    private ArrayList<Integer> player2Solution;
    int currentInstruction = 0;
    private boolean bothPlayersReady, player1Ready = false, player2Ready = false;
    private boolean player1Finished = false, player2Finished = false;
    private boolean instructionsMode = true;
    private boolean enterPattern = false;
    private boolean gameOver = false;
    private boolean repeatRound = false;
    private int round = 1;
    private boolean player1Wins;
    private int level = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = MemorygameBinding.inflate(getLayoutInflater());

        getSupportActionBar().hide();

        View view = binding.getRoot();
        setContentView(view);

        binding.blueButton1.setOnClickListener(this::switchView_1);
        binding.redButton1.setOnClickListener(this::switchView_1);
        binding.greenButton1.setOnClickListener(this::switchView_1);
        binding.yellowButton1.setOnClickListener(this::switchView_1);
        binding.blueButton2.setOnClickListener(this::switchView_2);
        binding.redButton2.setOnClickListener(this::switchView_2);
        binding.greenButton2.setOnClickListener(this::switchView_2);
        binding.yellowButton2.setOnClickListener(this::switchView_2);

        solution = generateRandomColorList(level);
        player1Solution = new ArrayList<>();
        player2Solution = new ArrayList<>();
    }

    private void fadeIn(boolean callOnClick){
        binding.textView1.setVisibility(View.INVISIBLE);
        binding.textView2.setVisibility(View.INVISIBLE);
        toggleAllButtons(false);
        AlphaAnimation alphaAnim = new AlphaAnimation(0.0f,1.0f);
        alphaAnim.setStartOffset(1000);                        // start in 5 seconds
        alphaAnim.setDuration(400);
        alphaAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if(callOnClick)
                {
                    binding.textView1.setText(R.string.ready);
                    binding.textView2.setText(R.string.ready);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.textView1.setVisibility(View.VISIBLE);
                binding.textView2.setVisibility(View.VISIBLE);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                toggleAllButtons(true);
                if(callOnClick)
                {
                    binding.blueButton2.callOnClick();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.textView1.setAnimation(alphaAnim);
        binding.textView2.setAnimation(alphaAnim);
    }

    private void fadeOut(){
        binding.textView1.setVisibility(View.VISIBLE);
        binding.textView2.setVisibility(View.VISIBLE);
        toggleAllButtons(false);
        AlphaAnimation alphaAnim = new AlphaAnimation(1.0f,0.0f);
        alphaAnim.setDuration(500);
        alphaAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.textView1.setVisibility(View.INVISIBLE);
                binding.textView2.setVisibility(View.INVISIBLE);
                toggleAllButtons(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.textView1.setAnimation(alphaAnim);
        binding.textView2.setAnimation(alphaAnim);
    }

    private void setText(int rule)
    {
        switch (rule)
        {
            case 1:
                binding.textView1.setText(R.string.rules_1);
                binding.textView2.setText(R.string.rules_1);
                break;
            case 2:
                binding.textView1.setText(R.string.rules_2);
                binding.textView2.setText(R.string.rules_2);
                break;
            case 3:
                binding.textView1.setText(R.string.rules_3);
                binding.textView2.setText(R.string.rules_3);
                break;
            case 4:
                binding.textView1.setText(R.string.rules_4);
                binding.textView2.setText(R.string.rules_4);
                break;
            default:
                break;
        }
    }

    private ArrayList<Integer> generateRandomColorList(int size)
    {
        if(size < 1) {
            return null;
        }

        ArrayList<Integer> res = new ArrayList<>();

        for(int i = 0; i < size; i++)
        {
            res.add(getRandomColor());
        }

        return res;
    }

    private int getRandomColor()
    {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 4);
        int colors[] = {Color.RED, Color.BLUE, Color.YELLOW, Color.GREEN};

        return colors[randomNum];
    }

    private void toggleAllButtons(boolean toggle)
    {
        binding.blueButton1.setClickable(toggle);
        binding.redButton1.setClickable(toggle);
        binding.greenButton1.setClickable(toggle);
        binding.yellowButton1.setClickable(toggle);
        binding.blueButton2.setClickable(toggle);
        binding.redButton2.setClickable(toggle);
        binding.greenButton2.setClickable(toggle);
        binding.yellowButton2.setClickable(toggle);
    }

    private void togglePlayer1Buttons(boolean toggle)
    {
        binding.blueButton1.setClickable(toggle);
        binding.redButton1.setClickable(toggle);
        binding.greenButton1.setClickable(toggle);
        binding.yellowButton1.setClickable(toggle);
    }

    private void togglePlayer2Buttons(boolean toggle)
    {
        binding.blueButton2.setClickable(toggle);
        binding.redButton2.setClickable(toggle);
        binding.greenButton2.setClickable(toggle);
        binding.yellowButton2.setClickable(toggle);
    }

    private void flashColors(ArrayList<Integer> solution)  {

        toggleAllButtons(false);

        final RelativeLayout layout = (RelativeLayout) binding.root;
        final AnimationDrawable drawable = new AnimationDrawable();

        binding.textView1.setText("Round "+ (round) + ": Wait for Colors to Stop Flashing\nThen Enter Color Pattern");
        binding.textView2.setText("Round "+ (round) + ": Wait for Colors to Stop Flashing\nThen Enter Color Pattern");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int color : solution)
        {
            drawable.addFrame(new ColorDrawable(color), 800);
            drawable.addFrame(new ColorDrawable(Color.BLACK), 400);
            drawable.setOneShot(true);
            layout.setBackgroundDrawable(drawable);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    drawable.start();

                }

            });
        }


        toggleAllButtons(true);

    }

    private void changeBackgroundColor(int on)
    {
        final RelativeLayout layout = (RelativeLayout) binding.root;
        final AnimationDrawable drawable = new AnimationDrawable();
        final Handler handler = new Handler();

        drawable.addFrame(new ColorDrawable(on), 400);
        drawable.addFrame(new ColorDrawable(Color.BLACK), 400);
        drawable.setOneShot(true);

        layout.setBackgroundDrawable(drawable);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawable.start();
            }
        }, 100);
    }

    private void switchView_1(View view) {

        if(instructionsMode)
        {
            switch(currentInstruction)
            {
                case 0:
                    switch(view.getId()) {
                        case R.id.blue_button_1:
                            player1Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 1;
                                fadeOut();

                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);
                            }
                            break;
                        case R.id.red_button_1:
                            player1Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 3;
                                fadeOut();
                                instructionsMode = false;
                                setText(currentInstruction + 1);
                                fadeIn(true);
                            }
                            break;
                    }

                    break;
                case 1:
                    switch(view.getId()) {
                        case R.id.blue_button_1:
                            player1Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 2;
                                fadeOut();

                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);
                            }
                            break;
                    }
                    break;
                case 2:
                    switch(view.getId()) {
                        case R.id.blue_button_1:
                            player1Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 3;
                                fadeOut();
                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);
                            }
                            break;
                    }
                    break;
                case 3:
                    switch(view.getId()) {
                        case R.id.blue_button_1:
                            player1Ready = true;
                            if (player1Ready && player2Ready) {
                                fadeOut();
                                binding.textView1.setText(R.string.ready);
                                binding.textView2.setText(R.string.ready);
                                instructionsMode = false;
                                fadeIn(true);
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                    }

                    break;
                default:
                    break;
            }

        }
        else
        {
            if(!enterPattern)
            {
                flashColors(solution);
                enterPattern = true;
                player1Ready = false;
                player2Ready = false;
            }
            else
            {
                if(player1Solution.size() < solution.size())
                {
                    switch(view.getId())
                    {
                        case R.id.blue_button_1:
                            player1Solution.add(Color.BLUE);
                            break;
                        case R.id.green_button_1:
                            player1Solution.add(Color.GREEN);
                            break;
                        case R.id.red_button_1:
                            player1Solution.add(Color.RED);
                            break;
                        case R.id.yellow_button_1:
                            player1Solution.add(Color.YELLOW);
                            break;
                        default:
                            break;
                    }

                    if(player1Solution.size()  == solution.size())
                    {
                        player1Finished = true;
                        binding.blueButton1.callOnClick();
                    }
                }
                else
                {
                    togglePlayer1Buttons(false);
                    if(player1Solution.equals(solution))
                    {
                        player1Ready = true;
                    }
                    else
                    {
                        player1Ready = false;
                    }

                    if(player2Finished)
                    {
                        if(player2Ready ^ player1Ready)
                        {
                            if(player2Ready)
                            {
                                binding.textView1.setText("You Lose\nPlayer1 Wins\nGame Over!");
                                binding.textView2.setText("You Win\nPlayer1 Wins\nGame Over!");
                                player1Wins = true;
                            }
                            else
                            {
                                binding.textView1.setText("You Win\nPlayer2 Wins\nGame Over!");
                                binding.textView2.setText("You Lost\nPlayer2 Wins\nGame Over!");
                                player1Wins = false;
                            }
                            repeatRound = false;

                            gameOver = true;
                        }
                        else
                        {
                            if(player1Ready)
                            {
                                binding.textView1.setText("Both Players Correct!\nNext Round!");
                                binding.textView2.setText("Both Players Correct!\nNext Round!");
                                repeatRound = false;
                            }
                            else
                            {
                                binding.textView1.setText("Both Players Lost\n Restarting Round");
                                binding.textView2.setText("Both Players Lost\n Restarting Round");
                                repeatRound = true;
                            }
                            player1Ready = true;
                            player2Ready = true;
                        }
                    } else
                    {
                        if(player1Ready)
                        {
                            binding.textView1.setText("Success! Waiting for Player1...");
                        }
                        else
                        {
                            binding.textView1.setText("Failure! Waiting for Player1...");
                        }
                        repeatRound = false;
                        return;
                    }

                    if(gameOver)
                    {
                        //pass over winner here
                        if(player1Wins)
                        {
                            //player 1 wins (TOP SCREEN)
                            Intent intent = new Intent();
                            intent.putExtra("winner", player1Wins);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                        else
                        {
                            //player 2 wins (BOTTOM SCREEN)
                            Intent intent = new Intent();
                            intent.putExtra("winner", player1Wins);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                    else
                    {
                        if(player1Ready && player2Ready)
                        {
                            if(repeatRound)
                            {

                            }
                            else
                            {
                                level++;
                                round++;
                            }
                            solution = generateRandomColorList(level);
                            instructionsMode = true;
                            currentInstruction = 3;
                            player1Solution.clear();
                            player2Solution.clear();
                            player1Ready = false;
                            player2Ready = false;
                            player1Finished = false;
                            player2Finished = false;
                            enterPattern = false;
                            setText(currentInstruction + 1);
                            toggleAllButtons(true);
                        }
                    }

                }
            }

        }
    }

    private void switchView_2(View view) {

        if(instructionsMode)
        {
            switch(currentInstruction)
            {
                case 0:
                    switch(view.getId()) {
                        case R.id.blue_button_2:
                            player2Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 1;
                                fadeOut();
                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);

                            }
                            break;
                        case R.id.red_button_2:
                            player2Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 3;
                                fadeOut();
                                instructionsMode = false;
                                setText(currentInstruction + 1);
                                fadeIn(true);
                            }
                            break;
                    }
                    break;
                case 1:
                    switch(view.getId()) {
                        case R.id.blue_button_2:
                            player2Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 2;
                                fadeOut();
                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);
                            }
                            break;
                    }
                    break;
                case 2:
                    switch(view.getId()) {
                        case R.id.blue_button_2:
                            player2Ready = true;
                            if (player1Ready && player2Ready) {
                                currentInstruction = 3;
                                fadeOut();
                                player1Ready = false;
                                player2Ready = false;
                                setText(currentInstruction + 1);
                                fadeIn(false);
                            }
                            break;
                    }
                    break;
                case 3:
                    switch(view.getId()) {
                        case R.id.blue_button_2:
                            player2Ready = true;
                            if (player1Ready && player2Ready) {
                                fadeOut();
                                binding.textView1.setText(R.string.ready);
                                binding.textView2.setText(R.string.ready);
                                instructionsMode = false;
                                fadeIn(true);
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                    }
                    break;
                default:
                    break;
            }

        }
        else
        {
            if(!enterPattern)
            {
                flashColors(solution);
                enterPattern = true;
                player1Ready = false;
                player2Ready = false;
            }
            else
            {
                if(player2Solution.size()  < solution.size())
                {
                    switch(view.getId())
                    {
                        case R.id.blue_button_2:
                            player2Solution.add(Color.BLUE);
                            break;
                        case R.id.green_button_2:
                            player2Solution.add(Color.GREEN);
                            break;
                        case R.id.red_button_2:
                            player2Solution.add(Color.RED);
                            break;
                        case R.id.yellow_button_2:
                            player2Solution.add(Color.YELLOW);
                            break;
                        default:
                            break;
                    }

                    if(player2Solution.size()  == solution.size())
                    {
                        player2Finished = true;
                        binding.blueButton2.callOnClick();
                    }
                }
                else
                {
                    togglePlayer2Buttons(false);
                    if(player2Solution.equals(solution))
                    {
                        player2Ready = true;
                    }
                    else
                    {
                        player2Ready = false;
                    }

                    if(player1Finished)
                    {
                        if(player2Ready ^ player1Ready)
                        {
                            if(player1Ready)
                            {

                                binding.textView1.setText("You Win\nPlayer2 Wins\nGame Over!");
                                binding.textView2.setText("You lose\nPlayer2 Wins\nGame Over!");
                                player1Wins = false;
                            }
                            else
                            {
                                binding.textView1.setText("You Lose\nPlayer1 Wins\nGame Over!");
                                binding.textView2.setText("You Win\nPlayer1 Wins\nGame Over!");
                                player1Wins = true;
                            }
                            gameOver = true;
                            repeatRound = false;
                        }
                        else
                        {
                            if(player1Ready)
                            {
                                binding.textView1.setText("Both Players Correct!\nNext Round!");
                                binding.textView2.setText("Both Players Correct!\nNext Round!");
                                repeatRound = false;
                            }
                            else
                            {
                                binding.textView1.setText("Both Players Lost\n Restarting Round");
                                binding.textView2.setText("Both Players Lost\n Restarting Round");
                                repeatRound = true;
                            }
                            player1Ready = true;
                            player2Ready = true;
                        }
                    } else
                    {
                        if(player2Ready)
                        {
                            binding.textView2.setText("Success! Waiting for Player2...");
                        }
                        else
                        {
                            binding.textView2.setText("Failure! Waiting for Player2...");
                        }
                        repeatRound = false;
                        return;
                    }

                    if(gameOver)
                    {
                        //pass over winner here
                        if(player1Wins)
                        {
                            //player 2 wins (TOP SCREEN)
                            Intent intent = new Intent();
                            intent.putExtra("winner", player1Wins);
                            setResult(RESULT_OK, intent);
                            finish();
                            Toast.makeText(this, "player1Wins:" + player1Wins, Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //player 1 wins (BOTTOM SCREEN)

                            Intent intent = new Intent();
                            intent.putExtra("winner", player1Wins);
                            setResult(RESULT_OK, intent);
                            finish();
                            Toast.makeText(this, "player1Wins:" + player1Wins, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        if(player1Ready && player2Ready)
                        {
                            if(repeatRound)
                            {

                            }
                            else
                            {
                                level++;
                                round++;
                            }
                            solution = generateRandomColorList(level);
                            instructionsMode = true;
                            player1Solution.clear();
                            player2Solution.clear();
                            player1Ready = false;
                            player2Ready = false;
                            player1Finished = false;
                            player2Finished = false;
                            enterPattern = false;
                            currentInstruction = 3;
                            setText(currentInstruction + 1);
                            toggleAllButtons(true);
                        }
                    }

                }
            }
        }
    }
}