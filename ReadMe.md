<h1><u>Team 5 Final Project</u></h1>

<h2> Project Name: Tic Tac Toe Game </h2>
<h3> Team Members </h4>
<ul>
	<li>Raul Cisneros Renteria</li>
	<li>Matthew Zuberbuhler</li>
	<li>Christian Santos</li>
	<li>Tae Ho Lee</li>
</ul>

<h3>Project Backlog</h3>

[Tic Tac Toe Backlog](https://docs.google.com/spreadsheets/d/1HPt5vA2CH3JKSFZeHC6_dqdj5dGjY4LhaZIS70Cco38/edit?usp=sharing)

<h3>APK File Link</h3>

[Tic Take Toe Game APK](https://drive.google.com/drive/folders/1nerCAMnEwVB_dWKKP8-2jhc0_LBCMwpM?usp=sharing)

<h3>Video Walkthrough Link</h3>

[Tic Take Toe Game Video Walkthrough](https://www.youtube.com/watch?v=XcF5s8tMgAs)

<h3>Image Preview</h3>

Main Menu

![Main Menu](resources/screenshot1.png)

Tic Tac Toe

![Tic Tac Toe](resources/screenshot2.png)

Flash Game

![Flash Game](resources/screenshot3.png)

Flappy Bird Game

![Flappy Bird Game](resources/screenshot4.png)

First To Zero Game

![First To Zero](resources/screenshot5.png)